import cherrypy
import random

from handlers import extended_info
from handlers import user_db_data


@cherrypy.popargs("user_id")
class Users(object):
    exposed = True

    def __init__(self):
        self.extended_info = extended_info.ExtendedInfo()

    @cherrypy.tools.json_out()
    def GET(self, user_id=None):
        if user_id:
            for user in user_db_data.user_db:
                if user.get('id') == user_id:
                    return user
        else:
            return user_db_data.user_db

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self):
        user_data = cherrypy.request.json
        user_data['id'] = str(random.randint(1000, 9999))
        user_db_data.user_db.append(user_data)
        return user_data

    @cherrypy.tools.json_out()
    def DELETE(self, user_id):
        for ind, user in enumerate(user_db_data.user_db):
            if user.get("id") == user_id:
                user_db_data.user_db.pop(ind)
        return {}
