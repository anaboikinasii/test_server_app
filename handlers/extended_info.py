import cherrypy
import random

from handlers.extended_info_db import db


class ExtendedInfo(object):
    exposed = True

    @cherrypy.tools.json_out()
    def GET(self, user_id):
        return db.get(user_id, {})

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self, user_id):
        extended_info_data = cherrypy.request.json
        extended_info_data['id'] = str(random.randint(1000, 9999))
        db[user_id] = extended_info_data
        return extended_info_data
