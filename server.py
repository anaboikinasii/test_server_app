import cherrypy

from handlers import users

USERS = {'jon': 'secret'}


def validate_password(realm, username, password):
    if username in USERS and USERS[username] == password:
        return True
    return False

conf = {
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.auth_basic.on': True,
        'tools.auth_basic.realm': 'localhost',
        'tools.auth_basic.checkpassword': validate_password
    },
    '/users': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.auth_basic.on': True,
        'tools.auth_basic.realm': 'localhost',
        'tools.auth_basic.checkpassword': validate_password
    }
}

cherrypy.tree.mount(users.Users(), '/users', conf)
cherrypy.quickstart()

